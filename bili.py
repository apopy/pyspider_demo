from urllib import request
from io import BytesIO
import gzip
from lxml import etree
import time
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36"
}
base_url = 'https://www.bilibili.com/video/'
#添加视频BV号
BV = ["BV1es411171r", "BV1zs411m7ht", "BV1Ss411x7FW", "BV1Xt4y1m7v3",]
list = []
for r in BV:
    url = base_url + r
    req = request.Request(url=url, headers=headers)
    resp = request.urlopen(req)
    #解压网页源码
    buff = BytesIO(resp.read())
    html = gzip.GzipFile(fileobj=buff).read().decode('utf-8')

    tree = etree.HTML(html)
    video_view = tree.xpath('//*[@id="viewbox_report"]/div/div/span[1]/@title')
    video_title = tree.xpath('//*[@id="viewbox_report"]/h1/@title')
    # 获取当前时间
    current_time = int(time.time())
    # 转换为localtime
    localtime = time.localtime(current_time)
    # 利用strftime()函数重新格式化时间
    t = time.strftime('%Y:%m:%d %H:%M:%S', localtime)
    list.append(t+','+video_title[0]+','+video_view[0]+'\n')

with open('record.txt', 'a', encoding='utf-8') as fp:
    for s in list:
        fp.write(s)
# print(res)
